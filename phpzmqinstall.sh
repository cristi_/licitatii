#!/bin/bash
cd /tmp
apt-get update
apt install -y libtool pkg-config build-essential autoconf automake uuid-dev git wget

git clone git://github.com/jedisct1/libsodium.git
cd libsodium
./autogen.sh
./configure && make check
sudo make install
sudo ldconfig
cd ..

git clone git://github.com/zeromq/libzmq.git
cd libzmq
./autogen.sh
./configure && make check
sudo make install
sudo ldconfig
cd ..

git clone git://github.com/zeromq/czmq.git
cd czmq
git checkout tags/v2.2.0
./autogen.sh
./configure && make check
sudo make install
sudo ldconfig
cd ..

apt -y install php5-dev
git clone https://github.com/mkoppanen/php-zmq.git
cd php-zmq/
phpize && ./configure
make && make test && make install
echo "extension=zmq.so" >> /etc/php5/cli/php.ini
echo "extension=zmq.so" >> /etc/php5/apache2/php.ini